# translation of kmines.po to Dutch
# translation of kmines.po to
# KTranslator Generated File
# Dutch Translation of KMines.po
# Copyright (C) 2000, 2001, 2002 KDE-Nederlands team <i18n@kde.nl>.
# Gelezen, Rinse
# Niels Reedijk <n.reedijk@planet.nl>, 2000.
# Rinse de Vries <rinse@kde.nl>, 2000, 2001, 2002, 2003, 2004.
# Bram Schoenmakers <bramschoenmakers@kde.nl>, 2004.
# Rinse de Vries <rinsedevries@kde.nl>, 2004, 2007.
# Kristof Bal <kristof.bal@gmail.com>, 2008.
# Freek de Kruijf <f.de.kruijf@hetnet.nl>, 2009.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2010, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kmines\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-15 00:44+0000\n"
"PO-Revision-Date: 2021-07-13 13:35+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Niels Reedijk,Rinse de Vries,Freek de Kruijf - 2019 t/m 2021"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ",rinse@kde.nl,freekdekruijf@kde.nl"

#. i18n: ectx: property (text), widget (QLabel, label)
#: customgame.ui:16
#, kde-format
msgid "Width:"
msgstr "Breedte:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: customgame.ui:26
#, kde-format
msgid "Height:"
msgstr "Hoogte:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: customgame.ui:36
#, kde-format
msgid "Mines:"
msgstr "Mijnen:"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_UseQuestionMarks)
#: generalopts.ui:17
#, kde-format
msgid "Use '?' marks"
msgstr "'?'-tekens gebruiken"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_AllowKminesReset)
#: generalopts.ui:24
#, kde-format
msgid "Allow KMines reset"
msgstr "Reset van KMines toestaan"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_DisableScoreOnReset)
#: generalopts.ui:37
#, kde-format
msgid "Disable score on reset"
msgstr "Score uitschakelen bij reset"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ExploreWithLeftClickOnNumberCells)
#: generalopts.ui:44
#, kde-format
msgid "Explore with left click on number cells"
msgstr "Met linker klik op aantal cellen zoeken"

#. i18n: ectx: label, entry (UseQuestionMarks), group (General)
#: kmines.kcfg:9
#, kde-format
msgid "Whether the \"unsure\" marker may be used."
msgstr "Of de vraagtekenmarkering mag worden gebruikt."

#. i18n: ectx: label, entry (ExploreWithLeftClickOnNumberCells), group (General)
#: kmines.kcfg:21
#, kde-format
msgid "Left click on a number cell will have the same effect as mid click."
msgstr ""
"Linker klik op een nummercel zal hetzelfde effect hebben als een middenklik."

#. i18n: ectx: label, entry (CustomWidth), group (Options)
#: kmines.kcfg:27
#, kde-format
msgid "The width of the playing field."
msgstr "De breedte van het speelveld."

#. i18n: ectx: label, entry (CustomHeight), group (Options)
#: kmines.kcfg:33
#, kde-format
msgid "The height of the playing field."
msgstr "De lengte van het speelveld."

#. i18n: ectx: label, entry (CustomMines), group (Options)
#: kmines.kcfg:39
#, kde-format
msgid "The number of mines in the playing field."
msgstr "Het aantal mijnen in het speelveld."

#. i18n: ectx: ToolBar (mainToolBar)
#: kminesui.rc:12
#, kde-format
msgid "Main Toolbar"
msgstr "Hoofdwerkbalk"

#: main.cpp:43
#, kde-format
msgid "KMines"
msgstr "KMines"

#: main.cpp:45
#, kde-format
msgid "KMines is a classic minesweeper game"
msgstr "KMines is een klassiek mijnenvegerspel"

#: main.cpp:47
#, kde-format
msgid ""
"(c) 1996-2005, Nicolas Hadacek\n"
"(c) 2001, Mikhail Kourinny\n"
"(c) 2006-2007, Mauricio Piacentini\n"
"(c) 2007, Dmitry Suzdalev"
msgstr ""
"(c) 1996-2005, Nicolas Hadacek\n"
"(c) 2001, Mikhail Kourinny\n"
"(c) 2006-2007, Mauricio Piacentini\n"
"(c) 2007, Dmitry Suzdalev"

#: main.cpp:50
#, kde-format
msgid "Nicolas Hadacek"
msgstr "Nicolas Hadacek"

#: main.cpp:51
#, kde-format
msgid "Original author"
msgstr "Oorspronkelijke auteur"

#: main.cpp:52
#, kde-format
msgid "Mauricio Piacentini"
msgstr "Mauricio Piacentini"

#: main.cpp:53
#, kde-format
msgid "Code refactoring and SVG support. Current maintainer"
msgstr "Refactorering van code en ondersteuning voor SVG. Huidige onderhouder"

#: main.cpp:55
#, kde-format
msgid "Dmitry Suzdalev"
msgstr "Dmitry Suzdalev"

#: main.cpp:56
#, kde-format
msgid "Rewrite to use QGraphicsView framework. Current maintainer"
msgstr "Herschrijvingen voor het QGraphicsView-raamwerk. Huidige onderhouder"

#: main.cpp:58
#, kde-format
msgid "Andreas Zehender"
msgstr "Andreas Zehender"

#: main.cpp:58
#, kde-format
msgid "Smiley pixmaps"
msgstr "Smiley-afbeeldingen"

#: main.cpp:59
#, kde-format
msgid "Mikhail Kourinny"
msgstr "Mikhail Kourinny"

#: main.cpp:59
#, kde-format
msgid "Solver/Adviser"
msgstr "Oplosser/adviesgever"

#: main.cpp:60
#, kde-format
msgid "Thomas Capricelli"
msgstr "Thomas Capricelli"

#: main.cpp:60
#, kde-format
msgid "Magic reveal mode"
msgstr "Automagisch onthullen"

#: main.cpp:61
#, kde-format
msgid "Brian Croom"
msgstr "Brian Croom"

#: main.cpp:61
#, kde-format
msgid "Port to use KGameRenderer"
msgstr "Code geport om KGameRenderer te gebruiken"

#: mainwindow.cpp:101
#, kde-format
msgid "Mines: 0/0"
msgstr "Mijnen: 0/0"

#: mainwindow.cpp:102 mainwindow.cpp:173
#, kde-format
msgid "Time: 00:00"
msgstr "Tijd: 00:00"

#: mainwindow.cpp:125
#, kde-format
msgid "Custom"
msgstr "Aangepast"

#: mainwindow.cpp:135
#, kde-format
msgid "Mines: %1/%2"
msgstr "Mijnen: %1/%2"

#: mainwindow.cpp:201
#, kde-format
msgid "Reset?"
msgstr "Resetten?"

#: mainwindow.cpp:201
#, kde-format
msgid "Reset the Game?"
msgstr "Het spel resetten?"

#: mainwindow.cpp:212
#, kde-format
msgid "Time: %1"
msgstr "Tijd: %1"

#: mainwindow.cpp:238
#, kde-format
msgid "General"
msgstr "Algemeen"

#: mainwindow.cpp:239
#, kde-format
msgid "Theme"
msgstr "Thema"

#: mainwindow.cpp:240
#, kde-format
msgid "Custom Game"
msgstr "Aangepast spel"

#: scene.cpp:122
#, kde-format
msgid "Game is paused."
msgstr "Spel gepauzeerd"

#: scene.cpp:130
#, kde-format
msgid "Congratulations! You have won!"
msgstr "Gefeliciteerd! U hebt gewonnen!"

#: scene.cpp:132
#, kde-format
msgid "You have lost."
msgstr "U hebt verloren."

#~ msgid "The graphical theme to be used."
#~ msgstr "Het te gebruiken grafische thema."

#~ msgid "The difficulty level."
#~ msgstr "De moeilijkheidsgraad."

#~ msgid "Failed to load \"%1\" theme. Please check your installation."
#~ msgstr "Thema \"%1\" kon niet worden geladen. Controleer uw installatie."

#~ msgid ""
#~ "Custom game can not be started.<br />Number of mines is too big for "
#~ "current field."
#~ msgstr ""
#~ "Het aangepaste spel kon niet worden geladen.<br />Het aantal mijnen is "
#~ "teveel voor het huidige veld."
